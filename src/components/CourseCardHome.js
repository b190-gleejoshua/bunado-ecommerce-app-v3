import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';

import wave1 from "./images/wave1.png"
import wave2 from "./images/wave2.png"
import wave3 from "./images/wave3.png"


function CourseCardHome() {
  return (
    <Container >
      <h1 className='text-center my-5 py-5' bg='dark'>Featured Courses</h1>
      <Row>
        <Col md={4}>
          <Card style={{ width: '20rem' }} className='m-2'>
            <Card.Img variant="top" src={wave1} className='p-2' />
            <Card.Body>
              <Card.Title>Mochanovs Wave/Lap 1</Card.Title>
              <Card.Text>
                 Beginner Freediving
              </Card.Text>
              <Button as={NavLink} to='/courses/62ebc0b7129d85581c60b359' exact>Details</Button>
            </Card.Body>
          </Card>
        </Col>
        <Col md={4}>
          <Card style={{ width: '20rem' }} className='m-2'>
            <Card.Img variant="top" src={wave2} className='p-2'/>
            <Card.Body>
              <Card.Title>Mochanovs Wave/Lap 2</Card.Title>
              <Card.Text>
                 Intermediate Freediving
              </Card.Text>
               <Button as={NavLink} to='/courses/62ebc135129d85581c60b35b' exact>Details</Button>
            </Card.Body>
          </Card>
        </Col>
        <Col md={4}>
          <Card style={{ width: '20rem' }} className='m-2'>
            <Card.Img variant="top" src={wave3} className='p-2'/>
            <Card.Body>
              <Card.Title>Mochanovs Wave/Lap 3</Card.Title>
              <Card.Text>
                 Master Freediving
              </Card.Text>
               <Button as={NavLink} to='/courses/62ebc15a129d85581c60b35f' exact>Details</Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default CourseCardHome;