import {Container, Card, Button, Row, Col, Image} from 'react-bootstrap';
import pcbfins from "./images/pcbfins.jpg"
import mask from "./images/mask.jpg"
import buoy1 from "./images/buoy1.jpg"

function GearTypes(){
	return(
		  <Container >
		    <h1 className='text-center my-5 py-5'>Gears and Accesories</h1>
		    <Row>
		      <Col md={4}>
		        <Card style={{ width: '20rem' }} className='m-2'>
		          <Card.Img variant="top" src={pcbfins} className='p-2' />
		          <Card.Body>
		            <Card.Title>Freediving fins</Card.Title>
		            
		          </Card.Body>
		        </Card>
		      </Col>
		      <Col md={4}>
		        <Card style={{ width: '20rem' }} className='m-2'>
		          <Card.Img variant="top" src={mask} className='p-2'/>
		          <Card.Body>
		            <Card.Title>Masks and Snorkels</Card.Title>
		           
		          </Card.Body>
		        </Card>
		      </Col>
		      <Col md={4}>
		        <Card style={{ width: '20rem' }} className='m-2'>
		          <Card.Img variant="top" src={buoy1} className='p-2'/>
		          <Card.Body>
		            <Card.Title>Accesories</Card.Title>
		         
		          </Card.Body>
		        </Card>
		      </Col>
		    </Row>
		  </Container>
		);
	
}

export default GearTypes;