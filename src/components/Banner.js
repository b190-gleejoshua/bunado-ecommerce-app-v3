import Carousel from 'react-bootstrap/Carousel';
import Image from 'react-bootstrap/Image';
import Container from 'react-bootstrap/Container'
import bannerImage1 from './images/bannerImage1.jpg'
import bannerImage2 from './images/bannerImage2.jpeg'
import bannerImage3 from './images/bannerImage3.jpg'



function Banner() {
  return (
    <Container fluid>
      <h1 className='text-center mt-5'>Welcome to IslaFreediving</h1>
      <Carousel className='my-5'>
      
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100 img-fluid img-height"
            src= {bannerImage1}
            alt="First slide" 
          />
        </Carousel.Item>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100 img-fluid img-height"
            src={bannerImage2}
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100 img-fluid img-height"
            src={bannerImage3}
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
    </Container>
  );
}

export default Banner;