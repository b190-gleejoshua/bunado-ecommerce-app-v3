import {Fragment, useEffect, useState} from 'react';
import ProductCard from '../components/ProductCard';
import {Row, Button} from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function UserViewProducts({productsData}) {

	const [products, setProducts] = useState([]);
	
	const [cart, setCart] = useState([]);

    useEffect(() => {
        const productsArr = productsData.map(product => {
        	if(product.isActive === true){
				return (
					<ProductCard productProp={product} key={product._id} />
				)
        	}else{
        		return null;
        	}
        });

     
        setProducts(productsArr);

    }, [productsData]);


	return(
		<Fragment>
			<Row className='align-items-center justify-content-center bgcolor'>
				{products}
			</Row>
		</Fragment>

		)
}