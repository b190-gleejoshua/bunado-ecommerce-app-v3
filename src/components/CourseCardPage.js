import {Card, Button, Row, Col, Container, Image} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';


export default function CourseCardPage({courseProp}){
	
	console.log(courseProp);

	const {name, description, price, _id} = courseProp


	return(
		
			<Col md={7} className='m-5'>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title className='text-center'>{name}</Card.Title>

						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>

						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>

						<Link className = 'btn btn-primary' to={`/courses/${_id}`}>Details</Link>
					</Card.Body>
				</Card>
			</Col>


	
		)
}

