import {Card, Button, Row, Col, Container, Image} from 'react-bootstrap';
import {useState} from 'react';
import UserViewProducts from '../components/UserViewProducts'
import CartView from '../components/CartView'
import '../App.css'
import ReactReadMoreReadLess from "react-read-more-read-less";


export default function ProductCard({productProp, handleAddProduct}){
	
	const {name, description, price, _id} = productProp
	const [cart, setCart] = useState([])

	
	return(
			<Col md={3} className='m-3'>
				<Card className="cardHighlight p-3 cardSize">
					<Card.Body>
						<Card.Title className='text-center'>{name}</Card.Title>

						<Card.Subtitle>Description:</Card.Subtitle>
						<ReactReadMoreReadLess readMoreclassName='readMore'
						readMoreText = "Read More 🢃"
						readLessText = "Read less 🢁"
						>{description}</ReactReadMoreReadLess>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>

						<Button onClick={() => {handleAddProduct(productProp)}}>Add to Cart</Button>
					</Card.Body>
				</Card>
			</Col>
		)
}

