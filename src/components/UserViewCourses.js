import {Fragment, useEffect, useState} from 'react';
import CourseCardPage from '../components/CourseCardPage';
import { Row} from 'react-bootstrap';

export default function UserViewCourses({coursesData}) {

    const [courses, setCourses] = useState([]);

    useEffect(() => {
        const coursesArr = coursesData.map(course => {
            // Returns active courses as "CourseCard" components
        	if(course.isActive === true){
				return (
					<CourseCardPage courseProp={course} key={course._id}/>
				)
        	}else{
        		return null;
        	}
        });

     
        setCourses(coursesArr);

    }, [coursesData]);


	return(
		<Fragment>
			<Row className=" mb-3 bgcolor lign-items-center justify-content-center">
				{courses}
			</Row>	
		</Fragment>

		)
}