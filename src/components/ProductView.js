import {useState, useEffect, useContext, Fragment} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView(){


	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const {productId} = useParams();
	const[name, setName] = useState("");
	const [description, setDescription] = useState(" ");
	const [price, setPrice] = useState(0);
	const [orderQuantity, setorderQuantity] = useState(0)


	const handleIncrement = () =>{
		if(orderQuantity < 10){
			setorderQuantity(prevCount => prevCount + 1)
		}
		
	}

	const handleDecrement = () =>{
		if(orderQuantity > 0){
			setorderQuantity(prevCount => prevCount - 1)
		}
			
	}


	const createOrder = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`,{
			method: "POST",
			headers:{
				'Content-type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				productId: productId,
				orderQuantity: orderQuantity
				

			})
		})
		.then(res=>res.json())
		.then(data =>{
			console.log(data)

			if(data === true){
				Swal.fire({
					title:"Added to Cart",
					icon:"success",
				})
			}else{
				Swal.fire({
					title: "Something went Wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(()=>{
		console.log(productId);


		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data=>{
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	

	return(
		<Container className = "mt-5">
			<Row>
				<Col lg = {{span:6, offset:3}}>
					<Card>
						<Card.Body className ="text-center">
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							{(user.id !== null)?
							<Fragment>
								<div>
									<Button variant ="primary" onClick={()=>handleDecrement(orderQuantity)} block>-</Button>
									<span style={{width:'5rem'}}>{orderQuantity}</span>
									<Button variant ="primary" onClick={()=>handleIncrement(orderQuantity)} block>+</Button>
								</div>
								<Button variant ="primary" onClick={()=>createOrder(productId)} block>Add to Cart</Button>
							</Fragment>
							:
							<Link className = 'btn btn-danger btn-block' to='/login'>Login to Buy</Link>
						}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)
}