import {useContext, Fragment } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';
import UserContext from './UserContext';

export default function AppNavbar() {

	const{user} = useContext(UserContext);
	console.log(user)
  return (
	    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" className ='p-3'>
	        <Navbar.Brand as={Link} to='/' bg="dark">IslaFreediving</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="responsive-navbar-nav">
	          <Nav className="me-auto" bg="dark">
	          	<Nav.Link as={NavLink} to='/' exact>Home</Nav.Link>
	            <Nav.Link as={NavLink} to='/courses' exact>Courses</Nav.Link>
	            <Nav.Link as={NavLink} to='/products'>Products</Nav.Link>
	          </Nav>
	          <Nav>
	          {(user.id!==null)?
	          	 <Fragment>
		          	 <Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
		          	 <Nav.Link as={NavLink} to='/cart'>Cart</Nav.Link>
	          	 </Fragment>
	          	:
	          	 <Fragment>
			            <Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
			            <Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
			          </Fragment>
			        }
	          </Nav>
	        </Navbar.Collapse>
	    </Navbar>
  );
}
