import './App.css';
import { useEffect, useState } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
// UserConext
import {UserProvider} from './UserContext'
// Web Components
import AppNavbar from './AppNavbar';
// Web Pages

import Home from "./pages/Home";
import Register from "./pages/register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Courses from './pages/Courses'
import CourseView from "./components/CourseView";
import Products from "./pages/Products"
import ProductView from "./components/ProductView";
import CartView from "./components/CartView";
import UserViewProducts from "./components/UserViewProducts"



function App(){

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  const [cart, setCart] = useState([]); 

  const handleAddProduct = (productProp) =>{
    console.log("handleAddProduct function called")
    const ProductExist = cart.find((item)=> item.id===productProp._id);
      if(ProductExist){
        setCart(cart.map((item)=> item.id === productProp._id?
          {...ProductExist, quantity: ProductExist.quantity + 1}: item))
      }else{
        setCart([...cart, {...productProp, quantity: 1}])
      }
       
  }


  return(
    <UserProvider value ={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container fluid>
        <Routes>
          <Route path='/' element={<Home/>} />    
          <Route path='/login' element={<Login/>} />
          <Route path='/register' element={<Register/>} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/courses' element={<Courses/>} />
          <Route path='/courses/:courseId' element={<CourseView />} />
          <Route path='/products' exact element={<Products/>} />
          <Route path='/products/:productId' element={<ProductView />} />
          <Route path='/cart' exact element={<CartView />} />
        </Routes>
        </Container>
      </Router>
    </UserProvider>
    )
}


export default App;