import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
// User COntext
import UserContext from '../UserContext';

// sweetalert2
import Swal from 'sweetalert2';

export default function Login() {

	const {user, setUser} = useContext(UserContext)

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const [ isActive, setIsActive] = useState(false);


	const retrieveUserDetails = (token) =>{
		fetch(`${ process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res =>res.json())
		.then(data => {
			console.log(data);


			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}
	useEffect(()=>{
		if (email !== ''&& password !== '') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);

	function loginUser(e){
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})

		.then(res => res.json())
		// good practice to output the response in the console to check if the codes are working
		.then(data => {
			console.log(data)

			if (typeof data.access !== 'undefined'){
				// JWT will be used to  retrieve user information accross the whole frontenD application
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login sucessful!",
					icon: "success",
					text: "Welcome to IslaFreediving!"

					})
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login credentials and try again"
				})
			}

		});

		setEmail('');
		setPassword('');

	}


	return(
		(user.id !== null)?
		<Navigate to='/'/>
		:
		<div className='d-flex justify-content-center align-items-center min-vh-100 bg-secondary'>
		  <Form className='rounded bg-light p-4' onSubmit={(e) => loginUser(e)}>
		    <Form.Group className='p-2' controlId="userEmail">
		      <Form.Label>Email address</Form.Label>
		      <Form.Control 
		      				type="email" 
		      				placeholder="Enter email" 
		      				value={email}
		      				onChange={e => setEmail(e.target.value)}
		      				required 
				/>
		      <Form.Text className="text-muted">
		        We'll never share your email with anyone else.
		      </Form.Text>
		    </Form.Group>

		    <Form.Group className='p-2' controlId="password1">
		      <Form.Label>Password</Form.Label>
		      <Form.Control
		      				type="password" 
		      				placeholder="Password" 
		      				value={password}
		      				onChange={e => setPassword(e.target.value)}
		      				required
				/>
		    </Form.Group>

		    {isActive ?
		    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		    :
		    <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		  </Form>
		 </div>
		);
}
