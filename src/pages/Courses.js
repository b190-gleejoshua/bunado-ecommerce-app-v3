import {Fragment, useEffect, useState, useContext} from 'react';
import CourseCardPage from '../components/CourseCardPage';
import AdminViewCourses from '../components/AdminViewCourses';
import UserViewCourses from '../components/UserViewCourses';
import UserContext from '../UserContext';


export default function Courses(){

	const { user } = useContext(UserContext);

	   const [courses, setCourses] = useState([]);

	   const fetchData = () => {

	       fetch(`${ process.env.REACT_APP_API_URL}/courses/all`)
	       .then(res => res.json())
	       .then(data => {

	           setCourses(data);

	       });

	   }

	   useEffect(() => {
	       fetchData();
	   }, []);
	   
	   return (
	       <Fragment>
	           {/* If the user is an admin, show the Admin View. If not, show the regular courses page. */}
	           {(user.isAdmin === true)
	               ? <AdminViewCourses coursesData={courses} fetchData={fetchData}/>
	               : <UserViewCourses coursesData={courses}/>
	           }
	       </Fragment>
	   )

}