import { Fragment } from 'react';

import Banner from '../components/Banner';
import CourseCardHome from '../components/CourseCardHome';
import ProductTypes from '../components/ProductTypes';

export default function Home(){
	return(
		<Fragment>
			<Banner/>
			<CourseCardHome/>
			<ProductTypes/>
		</Fragment>
	)
}
