import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();


		// state hooks to store the values of the input fields
		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [email, setEmail] = useState('');
		const [mobileNo, setMobileNo] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		// state to determine whether the submit button is enabled or not
		const [isActive, setIsActive] = useState(false);

		// to check if we have successfully implemented the 2-way binding
		/*
			whenever a state of a component changes, the component renders the whole component executing the code found in the component itself.
			e.target.value allows us to gain access the input field's current value to be used when submitting form data
		*/
		console.log(email);
		console.log(password1);
		console.log(password2);
		
	useEffect(()=>{
		if( ( firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password1 !== '' && password2 !== '' ) && ( password1 === password2 ) ){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [ firstName, lastName, mobileNo, email, password1, password2 ]);

	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data === true){
		    	Swal.fire({
		    		title: "Duplicate	email found",
		    		icon: "error",
		    		text: "Kindly provide another email address."
		    	})
		    }else{
		    	fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
		    	    method: "POST",
		    	    headers: {
		    	        'Content-Type': 'application/json'
		    	    },
		    	    body: JSON.stringify({
		    	        firstName: firstName,
		    	        lastName: lastName,
		    	        email: email,
		    	        mobileNo: mobileNo,
		    	        password: password1
		    	    })
		    	})
		    	.then(res => res.json())
		    	.then(data => {

		    	    console.log(data);

		    	    if (data === true) {

		    	        // Clear input fields
		    	        setFirstName('');
		    	        setLastName('');
		    	        setEmail('');
		    	        setMobileNo('');
		    	        setPassword1('');
		    	        setPassword2('');

		    	        Swal.fire({
		    	            title: 'Registration successful',
		    	            icon: 'success',
		    	            text: 'Welcome to IslaFreediving!'

		    	        });
	    	            navigate('/login')

		    	    } else {

		    	        Swal.fire({
		    	            title: 'Something wrong',
		    	            icon: 'error',
		    	            text: 'Please try again.'   
		    	        });

		    	    }
		    	})
		    }
		})
	}


	return (
		(user.id !== null) ?
		<Navigate to='/' />
		:
		<div className='d-flex justify-content-center align-items-center py-5 bg-secondary min-vh-100'>
		  <Form className="bg-light p-4 rounded" onSubmit={(e) => registerUser(e)}>
		    <Form.Group controlId="userfName">
		      <Form.Label>First Name</Form.Label>
		      <Form.Control 
		      				type="text" 
		      				placeholder="Enter first name" 
		      				value={firstName}
		      				onChange={e => setFirstName(e.target.value)}
		      				required 
				/>
		      
		    </Form.Group>

		    <Form.Group controlId="userLName">
		      <Form.Label>Last Name</Form.Label>
		      <Form.Control 
		      				type="text" 
		      				placeholder="Enter last name" 
		      				value={lastName}
		      				onChange={e => setLastName(e.target.value)}
		      				required 
				/>
		      
		    </Form.Group>

		    <Form.Group controlId="userEmail">
		      <Form.Label>Email address</Form.Label>
		      <Form.Control 
		      				type="email" 
		      				placeholder="Enter email" 
		      				value={email}
		      				onChange={e => setEmail(e.target.value)}
		      				required 
				/>
			  <Form.Text className="text-muted">
		        We'll never share your email with anyone else.
		      </Form.Text>
		    </Form.Group>

		    <Form.Group controlId="userMobileNo">
		      <Form.Label>Mobile Number</Form.Label>
		      <Form.Control 
		      				type="text" 
		      				placeholder="Enter mobile number" 
		      				value={mobileNo}
		      				onChange={e => setMobileNo(e.target.value)}
		      				required 
				/>
		     
		    </Form.Group>

		    <Form.Group controlId="password1">
		      <Form.Label>Password</Form.Label>
		      <Form.Control
		      				type="password" 
		      				placeholder="Password" 
		      				value={password1}
		      				onChange={e => setPassword1(e.target.value)}
		      				required
				/>
		    </Form.Group>

		    <Form.Group controlId="password2">
		      <Form.Label>Verify Password</Form.Label>
		      <Form.Control 
		      				type="password" 
		      				placeholder="Password" 
		      				value={password2}
		      				onChange={e => setPassword2(e.target.value)}
		      				required
				/>
		    </Form.Group>
		    {isActive ?
		    <Button className='py-2' variant="primary" type="submit" id="submitBtn">Submit</Button>
		    :
		    <Button className='py-2' variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		  </Form>
	  </div>
	);
}
