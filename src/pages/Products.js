import {Fragment, useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import {Col, Row} from 'react-bootstrap'
import AdminViewProducts from '../components/AdminViewProducts';
import UserViewProducts from '../components/UserViewProducts';
import UserContext from '../UserContext';

export default function Products({handleAddProduct}){

	const { user } = useContext(UserContext);

	   const [products, setProducts] = useState([]);

	   const fetchData = () => {

	       fetch(`${ process.env.REACT_APP_API_URL}/products/all`)
	       .then(res => res.json())
	       .then(data => {

	           setProducts(data);

	       });

	   }

	   useEffect(() => {
	       fetchData();
	   }, []);
	   
	   return (
	       <Fragment>
	           {/* If the user is an admin, show the Admin View. If not, show the regular courses page. */}
	           {(user.isAdmin === true)
	               ? <AdminViewProducts productsData={products} fetchData={fetchData}/>
	               : <UserViewProducts productsData={products} handleAddProduct={handleAddProduct}/>
	           }
	       </Fragment>
	   )

}